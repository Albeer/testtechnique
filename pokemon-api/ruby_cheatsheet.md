##Ruby Command Cheatsheet

* bundle install : Install the dependencies specified in your Gemfile
* bundle exec rspec : Executes the specs (tests)

##Rails Command Cheatsheet
* rails s : Launches a web server
* rails routes : Lists all defined routes
* rails db:migrate
* rake db:reset : Drops database, loads schema and seeds data
* rake db:seed : Seed database

###Rails Project
* rails new *api name* --api -T : Generates a new project
  *  --api argument to tell Rails that this is an API app
  * -T exclude Minitest (default testing framework)
* rails generate rspec:install : Initializes the *spec* directory (test directory)
* rails g model *model name* *column_name*:*type* *column_name*:*type* : Generates a model
* rails g controller *ContollerName* : Generates a controller