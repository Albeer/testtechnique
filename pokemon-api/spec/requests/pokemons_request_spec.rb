require 'rails_helper'

RSpec.describe 'Pokemons API', type: :request do
  let!(:pokemons) { create_list(:pokemon, 10) }
  let(:pokemon_id) { pokemons.first.id }
  let(:pokemon) { pokemons.first }

  describe 'GET /pokemons' do
    let(:headers) do 
      { "ACCEPT" => "application/json", "CONTENT-TYPE" => "application/json" }
    end
    before { get '/pokemons', headers: headers }

    it 'returns pokemons' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /pokemons/:id' do
    let(:headers) do 
      { "ACCEPT" => "application/json", "CONTENT-TYPE" => "application/json" }
    end
    before { get "/pokemons/#{pokemon_id}", headers: headers }

    context 'when the record exists' do
      it 'returns the Pokemon' do
        expect(json).not_to be_empty
        expect(json['name']).to eq(pokemon.name)
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:pokemon_id) { 1000 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
        expect(response.body).to match(/Couldn't find Pokemon/)
      end
    end
  end

  describe 'POST /pokemons' do
    let(:valid_attributes) do
      { number: 1, name: 'Bulbasaur', type1: 'Grass', type2: 'Poison', total: 318, hp: 45, attack: 49,
        defense: 49, sp_atk: 65, sp_def: 65, speed: 45, generation: 1, legendary: false }
    end

    context 'when the request is valid' do
      before { post '/pokemons', params: valid_attributes }

      it 'creates a Pokemon' do
        expect(json).not_to be_empty
        expect(json['name']).to eq('Bulbasaur')
        expect(response).to have_http_status(201)
      end
    end

    let(:invalid_attributes) do
      { name: 'Bulbasaur', type1: 'Grass', type2: 'Poison', total: 318, hp: 45, attack: 49,
        defense: 49, sp_atk: 65, sp_def: 65, speed: 45, generation: 1, legendary: false }
    end

    context 'when the request is invalid' do
      before { post '/pokemons', params: invalid_attributes }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
        expect(response.body)
          .to match(/Validation failed: Number can't be blank/)
      end
    end
  end

  describe 'PUT /pokemons/:id' do
    let(:valid_attributes) { { name: 'Ivysaur' } }

    context 'when the record exists' do
      before { put "/pokemons/#{pokemon_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /pokemons/:id' do
    before { delete "/pokemons/#{pokemon_id}" }

    it 'deletes the record' do
      expect(response).to have_http_status(204)
    end
  end
end
