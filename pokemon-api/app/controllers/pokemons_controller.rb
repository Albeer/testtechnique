class PokemonsController < ApplicationController
  layout 'standard'
  before_action :set_pokemon, only: %i[show update destroy]

  # GET /pokemons
  def index
    @pokemons = Pokemon.paginate(page: params[:page], :per_page => 15)
    respond_to do |format|
      format.html
      format.json { json_response(Pokemon.all) }
    end
  end

  # GET /pokemons/:id
  def show
    respond_to do |format|
      format.html
      format.json { json_response(@pokemon) }
    end
  end

  # POST /pokemons
  def create
    @pokemon = Pokemon.create!(pokemon_params)
    json_response(@pokemon, :created)
  end

  # PUT /pokemons/:id
  def update
    @pokemon.update(pokemon_params)
    head :no_content
  end

  # DELETE /pokemons/:id
  def destroy
    @pokemon.destroy
    head :no_content
  end

  private

  def pokemon_params
    params.permit( :number, :name, :type1, :type2, :total, :hp, :attack,
                  :defense, :sp_atk, :sp_def, :speed, :generation, :legendary )
  end

  def set_pokemon
    @pokemon = Pokemon.find(params[:id])
  end
end
