class Pokemon < ApplicationRecord
  validates_presence_of :number, :name, :type1, :total, :hp, :attack,
                        :defense, :sp_atk, :sp_def, :speed, :generation
  validates :legendary, inclusion: [true, false]
end
