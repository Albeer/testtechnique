require 'csv'

CSV.foreach(Rails.root.join('lib', 'seeds', 'pokemon.csv'), headers: true) do |row|
    Pokemon.create! do |pokemon|
        pokemon.number = row[0]
        pokemon.name = row[1]
        pokemon.type1 = row[2]
        pokemon.type2 = row[3]
        pokemon.total = row[4]
        pokemon.hp = row[5]
        pokemon.attack = row[6]
        pokemon.defense = row[7]
        pokemon.sp_atk = row[8]
        pokemon.sp_def = row[9]
        pokemon.speed = row[10]
        pokemon.generation = row[11]
        pokemon.legendary = row[12].downcase
    end
end